# Investor


## Running application
- Prerequisites `docker`, `java 21`
- Run dependencies using `docker-compose up -d`
- Run application `gradlew bootRun`
- Swagger doc: http://localhost:8080/swagger-ui.html

