package com.invest.alot.investor.loan;

import com.invest.alot.investor.investment.Investment;
import jakarta.validation.Valid;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/loans/v1")
public class LoanController {
  private final LoanService loanService;

  public LoanController(LoanService loanService) {
    this.loanService = loanService;
  }

  @GetMapping
  public ResponseEntity<List<ApiLoan>> getAllLoans() {
    // TODO add pageable and filters ?
    return ResponseEntity.ok(
        loanService.getAllLoans().stream()
            .map(
                loanProjection ->
                    ApiLoan.builder()
                        .id(loanProjection.getId())
                        .name(loanProjection.getName())
                        .state(loanProjection.getState())
                        .interestRate(loanProjection.getInterestRate())
                        .periodInMonths(loanProjection.getPeriodInMonths())
                        .targetAmount(loanProjection.getTargetAmount())
                        .investedAmount(loanProjection.getInvestedAmount())
                        .createdTime(loanProjection.getCreatedTime())
                        .build())
            .toList());
  }

  @GetMapping("/{id}")
  public ResponseEntity<ApiLoanDetails> getLoanById(@PathVariable UUID id) {
    var loan = loanService.getLoanById(id);
    return ResponseEntity.ok(toApiLoanDetails(loan));
  }

  @PostMapping
  public ResponseEntity<ApiLoanDetails> createLoan(@Valid @RequestBody LoanRequest request) {
    var loan = loanService.createLoan(request);
    return ResponseEntity.status(HttpStatus.CREATED).body(toApiLoanDetails(loan));
  }

  private ApiLoanDetails toApiLoanDetails(Loan loan) {
    var investments = loan.getInvestments();
    return ApiLoanDetails.builder()
        .id(loan.getId())
        .name(loan.getName())
        .state(loan.getState().getState())
        .interestRate(loan.getInterestRate())
        .periodInMonths(loan.getPeriodInMonths())
        .targetAmount(loan.getTargetAmount())
        .investedAmount(
            investments.stream()
                .map(Investment::getAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add))
        .createdTime(loan.getCreatedTime())
        .updatedTime(loan.getUpdatedTime())
        .investments(toApiLoanInvestments(investments))
        .build();
  }

  private List<ApiLoanInvestment> toApiLoanInvestments(List<Investment> investments) {
    // TODO could be optimized to lessen query amount to investment user, let's discuss which
    // options are good?
    return investments.stream().map(this::toApiLoanInvestment).toList();
  }

  private ApiLoanInvestment toApiLoanInvestment(Investment investment) {
    var user = investment.getUser();
    return ApiLoanInvestment.builder()
        .id(investment.getId())
        .userId(user.getId())
        .username(user.getUsername())
        .amount(investment.getAmount())
        .createdTime(investment.getCreatedTime())
        .build();
  }
}
