package com.invest.alot.investor.loan;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

public interface UserLoanProjection {

  UUID getId();

  String getName();

  String getState();

  BigDecimal getInterestRate();

  int getPeriodInMonths();

  BigDecimal getTargetAmount();

  BigDecimal getInvestedAmount();

  BigDecimal getUserInvestedAmount();

  int getUserInvestedCount();

  LocalDateTime getCreatedTime();
}
