package com.invest.alot.investor.loan;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ApiLoanDetails {
  private UUID id;
  private String name;
  private LoanState.State state;
  private BigDecimal interestRate;
  private int periodInMonths;
  private BigDecimal targetAmount;
  private BigDecimal investedAmount;
  private LocalDateTime createdTime;
  private LocalDateTime updatedTime;
  @Builder.Default private List<ApiLoanInvestment> investments = new ArrayList<>();
}
