package com.invest.alot.investor.loan;

import com.invest.alot.investor.data.Model;
import com.invest.alot.investor.investment.Investment;
import com.invest.alot.investor.time.ClockHolder;
import jakarta.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import lombok.*;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
@Entity
@SuperBuilder
@Table(schema = "investor", name = "loan")
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Loan extends Model {

  @Version private int version;
  private String name;
  private BigDecimal interestRate;
  private int periodInMonths;
  private BigDecimal targetAmount;

  @Builder.Default
  @OneToMany(mappedBy = "loan", orphanRemoval = true, cascade = CascadeType.ALL)
  private List<LoanState> states = new ArrayList<>();

  @Builder.Default
  @OneToMany(mappedBy = "loan", orphanRemoval = true, cascade = CascadeType.ALL)
  private List<Investment> investments = new ArrayList<>();

  @Transient
  public LoanState getState() {
    return states.stream().max(Comparator.comparing(LoanState::getCreatedTime)).orElseThrow();
  }

  public boolean isClosed() {
    return LoanState.State.CLOSED.equals(getState().getState());
  }

  public void open() {
    addState(LoanState.builder().loan(this).state(LoanState.State.OPEN).build());
  }

  public void close() {
    addState(LoanState.builder().loan(this).state(LoanState.State.CLOSED).build());
  }

  private void addState(LoanState state) {
    log.info("Setting loan {} to {} state", this.getId(), state.getState());
    getStates().add(state);
    setUpdatedTime(LocalDateTime.now(ClockHolder.getClock()));
  }
}
