package com.invest.alot.investor.loan;

import java.util.List;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface LoanRepository extends JpaRepository<Loan, UUID> {
  @Query(
      nativeQuery = true,
      value =
          """
                  SELECT
                      loan.id AS id,
                      loan.name AS name,
                      loan_state.last_state AS state,
                      loan.interest_rate AS interestRate,
                      loan.period_in_months AS periodInMonths,
                      loan.target_amount AS targetAmount,
                      loan.created_time AS createdTime,
                      COALESCE(SUM(investment.amount), 0) AS investedAmount
                  FROM investor.loan loan
                  JOIN (
                      SELECT loan_id, state AS last_state,
                             ROW_NUMBER() OVER (PARTITION BY loan_id ORDER BY MAX(created_time) DESC) AS rn
                      FROM investor.loan_state
                      GROUP BY loan_id, state
                  ) AS loan_state ON loan_state.loan_id = loan.id and rn = 1
                  LEFT JOIN investor.investment investment ON investment.loan_id = loan.id
                  GROUP BY loan.id, loan.name, loan_state.last_state, loan.interest_rate, loan.period_in_months, loan.target_amount,loan.created_time
                  ORDER BY loan.created_time DESC
                  """)
  List<LoanProjection> findAllLoans();

  @Query(
      nativeQuery = true,
      value =
          """
                  SELECT
                      loan.id AS id,
                      loan.name AS name,
                      loan_state.last_state AS state,
                      loan.interest_rate AS interestRate,
                      loan.period_in_months AS periodInMonths,
                      loan.target_amount AS targetAmount,
                      loan.created_time AS createdTime,
                      COALESCE(SUM(investment.amount), 0) AS investedAmount,
                      COALESCE(SUM(CASE WHEN investment.user_id = :userId THEN investment.amount ELSE 0 END), 0) AS userInvestedAmount,
                      COALESCE(COUNT(CASE WHEN investment.user_id = :userId THEN investment.id ELSE NULL END), 0) AS userInvestedCount
                  FROM investor.loan loan
                  JOIN (
                      SELECT loan_id, state AS last_state,
                             ROW_NUMBER() OVER (PARTITION BY loan_id ORDER BY MAX(created_time) DESC) AS rn
                      FROM investor.loan_state
                      GROUP BY loan_id, state
                  ) AS loan_state ON loan_state.loan_id = loan.id and rn = 1
                  LEFT JOIN investor.investment investment ON investment.loan_id = loan.id
                  WHERE EXISTS(SELECT 1 FROM investor.investment userInvestment WHERE userInvestment.user_id = :userId AND userInvestment.loan_id = loan.id)
                  GROUP BY loan.id, loan.name, loan_state.last_state, loan.interest_rate, loan.period_in_months, loan.target_amount, loan.created_time
                  ORDER BY loan.created_time DESC
                  """)
  List<UserLoanProjection> findAllLoansByUserId(@Param("userId") UUID userId);
}
