package com.invest.alot.investor.loan;

import com.invest.alot.investor.exception.ClientException;
import com.invest.alot.investor.exception.ErrorCode;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class LoanService {
  private final LoanRepository loanRepository;

  public List<LoanProjection> getAllLoans() {
    return loanRepository.findAllLoans();
  }

  public List<UserLoanProjection> getAllUserLoans(UUID userId) {
    return loanRepository.findAllLoansByUserId(userId);
  }

  public Loan getLoanById(UUID id) {
    return loanRepository
        .findById(id)
        .orElseThrow(() -> new ClientException(ErrorCode.INVALID_LOAN_ID, id));
  }

  @Transactional
  public Loan createLoan(LoanRequest request) {
    var loan =
        loanRepository.save(
            Loan.builder()
                .name(request.getName())
                .interestRate(request.getInterestRate())
                .periodInMonths(request.getPeriodInMonths())
                .targetAmount(request.getTargetAmount())
                .build());
    loan.open();
    return loan;
  }
}
