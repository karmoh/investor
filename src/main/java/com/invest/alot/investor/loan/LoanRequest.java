package com.invest.alot.investor.loan;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LoanRequest {
  @NotBlank private String name;

  @NotNull
  @Min(1)
  private BigDecimal interestRate;

  @NotNull
  @Min(1)
  private int periodInMonths;

  @NotNull
  @Min(1)
  private BigDecimal targetAmount;
}
