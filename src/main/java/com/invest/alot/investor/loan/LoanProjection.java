package com.invest.alot.investor.loan;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

public interface LoanProjection {

  UUID getId();

  String getName();

  String getState();

  BigDecimal getInterestRate();

  int getPeriodInMonths();

  BigDecimal getTargetAmount();

  BigDecimal getInvestedAmount();

  LocalDateTime getCreatedTime();
}
