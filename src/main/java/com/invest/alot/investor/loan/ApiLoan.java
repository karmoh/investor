package com.invest.alot.investor.loan;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ApiLoan {
  private UUID id;
  private String name;
  private String state;
  private BigDecimal interestRate;
  private int periodInMonths;
  private BigDecimal targetAmount;
  private BigDecimal investedAmount;
  private LocalDateTime createdTime;
}
