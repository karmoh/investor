package com.invest.alot.investor.loan;

import com.invest.alot.investor.data.ImmutableModel;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.SuperBuilder;

@Entity
@SuperBuilder
@Table(schema = "investor", name = "loan_state")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class LoanState extends ImmutableModel {

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "loan_id", updatable = false)
  private Loan loan;

  @Enumerated(EnumType.STRING)
  private State state;

  public enum State {
    OPEN,
    CLOSED
  }
}
