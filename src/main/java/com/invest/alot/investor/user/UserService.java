package com.invest.alot.investor.user;

import com.invest.alot.investor.exception.ClientException;
import com.invest.alot.investor.exception.ErrorCode;
import com.invest.alot.investor.loan.LoanService;
import com.invest.alot.investor.loan.UserLoanProjection;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class UserService {
  private final UserRepository userRepository;
  private final LoanService loanService;

  public Page<User> getAllUsers(Pageable pageable) {
    return userRepository.findAll(pageable);
  }

  public User getUserById(UUID id) {
    return userRepository
        .findById(id)
        .orElseThrow(() -> new ClientException(ErrorCode.INVALID_USER_ID, id));
  }

  public ApiUserDetails getUserDetails(UUID id) {
    var user = getUserById(id);
    var userLoans = loanService.getAllUserLoans(user.getId());
    return ApiUserDetails.builder()
        .id(user.getId())
        .username(user.getUsername())
        .loans(toApiUserLoans(userLoans))
        .build();
  }

  private static List<ApiUserLoan> toApiUserLoans(List<UserLoanProjection> userLoans) {
    return userLoans.stream()
        .map(
            userLoan ->
                ApiUserLoan.builder()
                    .id(userLoan.getId())
                    .name(userLoan.getName())
                    .interestRate(userLoan.getInterestRate())
                    .periodInMonths(userLoan.getPeriodInMonths())
                    .targetAmount(userLoan.getTargetAmount())
                    .totalInvestedAmount(userLoan.getInvestedAmount())
                    .userInvestedAmount(userLoan.getUserInvestedAmount())
                    .userInvestedCount(userLoan.getUserInvestedCount())
                    .state(userLoan.getState())
                    .build())
        .toList();
  }

  @Transactional
  public ApiUserDetails createUser(UserRequest request) {
    var user = userRepository.save(User.builder().username(request.getUsername()).build());
    return ApiUserDetails.builder().id(user.getId()).username(user.getUsername()).build();
  }
}
