package com.invest.alot.investor.user;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ApiUserDetails {
  private UUID id;
  private String username;
  @Builder.Default private List<ApiUserLoan> loans = new ArrayList<>();
}
