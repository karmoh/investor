package com.invest.alot.investor.user;

import com.invest.alot.investor.data.Model;
import com.invest.alot.investor.investment.Investment;
import jakarta.persistence.*;
import java.util.ArrayList;
import java.util.List;
import lombok.*;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
@Entity
@SuperBuilder
@Table(schema = "investor", name = "user")
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class User extends Model {

  private String username;

  @Builder.Default
  @OneToMany(mappedBy = "user", orphanRemoval = true, cascade = CascadeType.ALL)
  private List<Investment> investments = new ArrayList<>();
}
