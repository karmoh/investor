package com.invest.alot.investor.user;

import com.invest.alot.investor.data.Page;
import com.invest.alot.investor.data.PageableUtils;
import jakarta.validation.Valid;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users/v1")
@RequiredArgsConstructor
public class UserController {
  private final UserService userService;

  @GetMapping
  public ResponseEntity<Page<ApiUser>> getUsers(
      @RequestParam(value = "_pageNumber", required = false, defaultValue = "0") Integer pageNumber,
      @RequestParam(value = "_pageSize", required = false, defaultValue = "25") Integer pageSize,
      @RequestParam(value = "_sort", required = false, defaultValue = "username")
          List<String> sort) {
    // TODO add filters?
    var users = userService.getAllUsers(PageableUtils.pageable(pageNumber, pageSize, sort));
    return ResponseEntity.ok(
        new Page<>(
            toApiUsers(users.getContent()),
            users.getTotalElements(),
            pageSize,
            users.getTotalPages()));
  }

  @GetMapping("/{id}")
  public ResponseEntity<ApiUserDetails> getUserDetails(@PathVariable UUID id) {
    var user = userService.getUserDetails(id);
    return ResponseEntity.ok(user);
  }

  @PostMapping
  public ResponseEntity<ApiUserDetails> createUser(@Valid @RequestBody UserRequest request) {
    return ResponseEntity.status(HttpStatus.CREATED).body(userService.createUser(request));
  }

  private List<ApiUser> toApiUsers(List<User> users) {
    return users.stream()
        .map(user -> ApiUser.builder().id(user.getId()).username(user.getUsername()).build())
        .toList();
  }
}
