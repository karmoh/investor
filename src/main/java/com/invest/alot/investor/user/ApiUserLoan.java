package com.invest.alot.investor.user;

import java.math.BigDecimal;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ApiUserLoan {
  private UUID id;
  private String name;
  private BigDecimal interestRate;
  private int periodInMonths;
  private BigDecimal targetAmount;
  private BigDecimal totalInvestedAmount;
  private BigDecimal userInvestedAmount;
  private int userInvestedCount;
  private String state;
}
