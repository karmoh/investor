package com.invest.alot.investor.data;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Page<T> {
  private List<T> data;
  private long total;
  private long pageNumber;
  private long pageSize;
}
