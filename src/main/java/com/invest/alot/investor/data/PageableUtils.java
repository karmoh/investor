package com.invest.alot.investor.data;

import java.util.List;
import java.util.stream.Collectors;
import lombok.experimental.UtilityClass;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.util.CollectionUtils;

@UtilityClass
public class PageableUtils {

  public static Pageable pageable(Integer pageNumber, Integer pageSize, List<String> sort) {
    if (CollectionUtils.isEmpty(sort)) {
      return PageRequest.of(pageNumber, maxPageSizeSet(pageSize));
    }
    return PageRequest.of(pageNumber, maxPageSizeSet(pageSize), mapSort(sort));
  }

  private static Sort mapSort(List<String> sort) {
    return Sort.by(
        sort.stream()
            .map(
                s -> {
                  if (s.startsWith("-")) {
                    return Sort.Order.desc(s.substring(1));
                  }
                  return Sort.Order.asc(s);
                })
            .collect(Collectors.toList()));
  }

  private Integer maxPageSizeSet(Integer pageSize) {
    if (pageSize == null || pageSize > 1000) {
      return 1000;
    }
    return pageSize;
  }
}
