package com.invest.alot.investor.exception;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ErrorMessage {
  private String errorCode;
  private String message;
  private LocalDateTime dateTime;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private Object[] arguments;
}
