package com.invest.alot.investor.exception;

public enum ErrorCode {
  TECHNICAL_EXCEPTION,
  INVALID_PARAMS,
  INVALID_USER_ID,
  INVALID_LOAN_ID,
  LOAN_CLOSED;

  public String getErrorCode() {
    return this.name().toLowerCase();
  }
}
