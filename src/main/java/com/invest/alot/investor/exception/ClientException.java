package com.invest.alot.investor.exception;

import lombok.Getter;

@Getter
public class ClientException extends RuntimeException {
  private final ErrorCode errorCode;
  private final Object[] arguments;

  public ClientException(ErrorCode errorCode, Object... arguments) {
    super(errorCode.getErrorCode());
    this.errorCode = errorCode;
    this.arguments = arguments;
  }
}
