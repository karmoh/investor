package com.invest.alot.investor.exception;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;

@Slf4j
@ControllerAdvice
@AllArgsConstructor
@RequestMapping(produces = "application/json")
public class ErrorControllerAdvice {
  private final Clock clock;
  private final MessageSource messageSource;

  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<ErrorMessage> bindException(final MethodArgumentNotValidException e) {
    List<String> errors = new ArrayList<>();
    e.getBindingResult()
        .getAllErrors()
        .forEach(
            fieldError -> {
              FieldError error = (FieldError) fieldError;
              errors.add(error.getField() + " " + error.getDefaultMessage());
            });
    return error(
        e,
        ErrorCode.INVALID_PARAMS.getErrorCode(),
        String.join(", ", errors),
        HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(IllegalArgumentException.class)
  public ResponseEntity<ErrorMessage> apiException(final IllegalArgumentException e) {
    return error(
        e, ErrorCode.INVALID_PARAMS.getErrorCode(), e.getMessage(), HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler({TechnicalException.class, Exception.class})
  public ResponseEntity<ErrorMessage> technicalException(final Exception exception) {
    return error(
        exception,
        ErrorCode.TECHNICAL_EXCEPTION.getErrorCode(),
        "Technical exception",
        HttpStatus.INTERNAL_SERVER_ERROR);
  }

  private ResponseEntity<ErrorMessage> error(
      final Exception exception, String errorCode, String message, HttpStatus httpStatus) {
    return error(exception, errorCode, message, httpStatus, null);
  }

  @ExceptionHandler(ClientException.class)
  public ResponseEntity<ErrorMessage> clientException(final ClientException e) {
    return error(
        e,
        e.getErrorCode().getErrorCode(),
        messageSource.getMessage(
            e.getErrorCode().name(), e.getArguments(), e.getMessage(), Locale.forLanguageTag("en")),
        HttpStatus.BAD_REQUEST,
        e.getArguments());
  }

  private ResponseEntity<ErrorMessage> error(
      final Exception exception,
      String errorCode,
      String message,
      HttpStatus httpStatus,
      Object[] arguments) {

    var responseMessage =
        ErrorMessage.builder()
            .errorCode(errorCode)
            .message(message)
            .dateTime(LocalDateTime.now(clock))
            .arguments(arguments)
            .build();

    log.error("{}", responseMessage, exception);
    return new ResponseEntity<>(responseMessage, httpStatus);
  }
}
