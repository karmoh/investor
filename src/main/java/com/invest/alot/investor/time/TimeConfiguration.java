package com.invest.alot.investor.time;

import java.time.Clock;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile({"!test"})
public class TimeConfiguration {
  @Bean
  public Clock clock() {
    return ClockHolder.getClock();
  }
}
