package com.invest.alot.investor.time;

import java.time.Clock;
import lombok.experimental.UtilityClass;

@UtilityClass
public class ClockHolder {
  private Clock CLOCK = Clock.systemUTC();

  public Clock getClock() {
    return CLOCK;
  }

  public void setClock(Clock clock) {
    CLOCK = clock;
  }
}
