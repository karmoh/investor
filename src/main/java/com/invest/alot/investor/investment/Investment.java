package com.invest.alot.investor.investment;

import com.invest.alot.investor.data.Model;
import com.invest.alot.investor.loan.Loan;
import com.invest.alot.investor.user.User;
import jakarta.persistence.*;
import java.math.BigDecimal;
import lombok.*;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
@Entity
@SuperBuilder
@Table(schema = "investor", name = "investment")
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Investment extends Model {

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "user_id", updatable = false)
  private User user;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "loan_id", updatable = false)
  private Loan loan;

  private BigDecimal amount;
}
