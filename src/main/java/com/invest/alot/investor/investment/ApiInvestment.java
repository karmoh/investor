package com.invest.alot.investor.investment;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ApiInvestment {
  private UUID id;
  private UUID userId;
  private String username;
  private BigDecimal amount;
  private LocalDateTime createdTime;
}
