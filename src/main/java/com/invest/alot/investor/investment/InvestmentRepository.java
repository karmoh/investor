package com.invest.alot.investor.investment;

import java.math.BigDecimal;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface InvestmentRepository extends JpaRepository<Investment, UUID> {

  @Query(
      """
    SELECT SUM(investment.amount) FROM Investment investment
    WHERE  investment.loan.id = :loanId
""")
  BigDecimal getInvestmentSumByLoanId(@Param("loanId") UUID loanId);
}
