package com.invest.alot.investor.investment;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InvestmentRequest {
  @NotNull private UUID userId;
  @NotNull private UUID loanId;

  @NotNull
  @Min(1)
  private BigDecimal amount;
}
