package com.invest.alot.investor.investment;

import com.invest.alot.investor.exception.ClientException;
import com.invest.alot.investor.exception.ErrorCode;
import com.invest.alot.investor.loan.Loan;
import com.invest.alot.investor.loan.LoanService;
import com.invest.alot.investor.socket.MessagePublisher;
import com.invest.alot.investor.user.UserService;
import java.math.BigDecimal;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationManager;

@Service
@RequiredArgsConstructor
public class InvestmentService {

  private static final String INVESTMENT_AMOUNT_TOPIC_PREFIX = "/topic/investedAmount/";

  private final InvestmentRepository investmentRepository;
  private final UserService userService;
  private final LoanService loanService;
  private final MessagePublisher messagePublisher;

  @Transactional
  public Investment investInLoan(InvestmentRequest request) {
    var loan = loanService.getLoanById(request.getLoanId());
    validateLoan(loan);
    var investment = createInvestment(request, loan);
    var loanInvestmentSum = getLoanInvestmentSumAndValidateAmountForLoanClosure(loan);
    publishLoanInvestmentAmountMessage(loan.getId(), loanInvestmentSum);
    return investment;
  }

  private void publishLoanInvestmentAmountMessage(UUID loanId, BigDecimal loanInvestmentSum) {
    TransactionSynchronizationManager.registerSynchronization(
        new TransactionSynchronization() {
          @Override
          public void afterCommit() {
            messagePublisher.publishMessage(
                INVESTMENT_AMOUNT_TOPIC_PREFIX + loanId, loanInvestmentSum);
          }
        });
  }

  private BigDecimal getLoanInvestmentSumAndValidateAmountForLoanClosure(Loan loan) {
    var loanInvestmentSum = investmentRepository.getInvestmentSumByLoanId(loan.getId());
    if (loanInvestmentSum.compareTo(loan.getTargetAmount()) >= 0) {
      loan.close();
    }
    return loanInvestmentSum;
  }

  private Investment createInvestment(InvestmentRequest request, Loan loan) {
    return investmentRepository.save(
        Investment.builder()
            .user(userService.getUserById(request.getUserId()))
            .loan(loan)
            .amount(request.getAmount())
            .build());
  }

  private static void validateLoan(Loan loan) {
    if (loan.isClosed()) {
      throw new ClientException(ErrorCode.LOAN_CLOSED, loan.getName(), loan.getId());
    }
  }
}
