package com.invest.alot.investor.investment;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/investments/v1")
@RequiredArgsConstructor
public class InvestmentController {
  private final InvestmentService investmentService;

  @PostMapping
  public ResponseEntity<ApiInvestment> createInvestment(
      @Valid @RequestBody InvestmentRequest request) {
    var investment = investmentService.investInLoan(request);
    return ResponseEntity.status(HttpStatus.CREATED).body(toApiInvestment(investment));
  }

  private ApiInvestment toApiInvestment(Investment investment) {
    var user = investment.getUser();
    return ApiInvestment.builder()
        .id(investment.getId())
        .userId(user.getId())
        .username(user.getUsername())
        .amount(investment.getAmount())
        .createdTime(investment.getCreatedTime())
        .build();
  }
}
