package com.invest.alot.investor.socket;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class MessagePublisher {
  private final SimpMessagingTemplate messagingTemplate;

  @Transactional
  public void publishMessage(String topic, Object message) {
    log.info("Publishing message to topic {} ; {}", topic, message);
    messagingTemplate.convertAndSend(topic, message);
  }
}
