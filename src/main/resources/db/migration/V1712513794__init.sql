CREATE OR REPLACE FUNCTION grant_table_privileges(_table text, _role text)
    RETURNS void
    LANGUAGE plpgsql AS
$func$
BEGIN
    EXECUTE format('GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE %s TO %I', _table, _role);
END
$func$;

CREATE OR REPLACE FUNCTION grant_table_immutable_delete_privileges(_table text, _role text)
    RETURNS void
    LANGUAGE plpgsql AS
$func$
BEGIN
    EXECUTE format('GRANT SELECT, INSERT ON TABLE %s TO %I', _table, _role);
END
$func$;


create table investor.user
(
    id           UUID PRIMARY KEY default uuid_generate_v4(),
    username     TEXT NOT NULL,
    created_time TIMESTAMP        DEFAULT CURRENT_TIMESTAMP,
    updated_time TIMESTAMP        DEFAULT CURRENT_TIMESTAMP
);

select grant_table_privileges('investor.user', 'investor-application-user');


create table investor.loan
(
    id               UUID PRIMARY KEY default uuid_generate_v4(),
    version          INTEGER NOT NULL DEFAULT 1,
    name             TEXT    NOT NULL,
    interest_rate    NUMERIC NOT NULL,
    period_in_months INTEGER NOT NULL,
    target_amount    NUMERIC NOT NULL,
    created_time     TIMESTAMP        DEFAULT CURRENT_TIMESTAMP,
    updated_time     TIMESTAMP        DEFAULT CURRENT_TIMESTAMP
);

select grant_table_privileges('investor.loan', 'investor-application-user');


create table investor.loan_state
(
    id           UUID PRIMARY KEY default uuid_generate_v4(),
    loan_id      UUID NOT NULL REFERENCES investor.loan (id),
    state        TEXT NOT NULL,
    created_time TIMESTAMP        DEFAULT CURRENT_TIMESTAMP
);

CREATE INDEX IF NOT EXISTS idx_loan_state_loan_id
    ON investor.loan_state (loan_id);

CREATE INDEX IF NOT EXISTS idx_loan_state_created_time
    ON investor.loan_state (created_time);

select grant_table_immutable_delete_privileges('investor.loan_state', 'investor-application-user');


create table investor.investment
(
    id           UUID PRIMARY KEY default uuid_generate_v4(),
    user_id      UUID    NOT NULL REFERENCES investor.user (id),
    loan_id      UUID    NOT NULL REFERENCES investor.loan (id),
    amount       NUMERIC NOT NULL,
    created_time TIMESTAMP        DEFAULT CURRENT_TIMESTAMP,
    updated_time TIMESTAMP        DEFAULT CURRENT_TIMESTAMP
);

CREATE INDEX IF NOT EXISTS idx_investment_loan_id
    ON investor.investment (loan_id);

CREATE INDEX IF NOT EXISTS idx_investment_user_id
    ON investor.investment (user_id);

select grant_table_immutable_delete_privileges('investor.investment', 'investor-application-user');
