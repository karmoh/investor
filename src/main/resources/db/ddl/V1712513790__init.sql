-- database
CREATE DATABASE "investor";

\connect investor;

SET TIMEZONE = 'UTC';

CREATE SCHEMA investor;

-- migration user
CREATE ROLE "investor-migration-user";
GRANT CONNECT ON DATABASE "investor" TO "investor-migration-user";

GRANT USAGE, CREATE ON SCHEMA public TO "investor-migration-user";
GRANT USAGE, CREATE ON SCHEMA investor TO "investor-migration-user";

CREATE USER "investor-flyway" WITH PASSWORD 'investor-flyway';
GRANT "investor-migration-user" TO "investor-flyway";

-- application user
CREATE ROLE "investor-application-user";
GRANT CONNECT ON DATABASE "investor" TO "investor-application-user";

GRANT USAGE ON SCHEMA investor TO "investor-application-user";

GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA investor TO "investor-application-user";

CREATE USER "investor" WITH PASSWORD 'investor';
GRANT "investor-application-user" TO "investor";

REVOKE CREATE ON SCHEMA public FROM PUBLIC;
REVOKE CREATE ON SCHEMA investor FROM PUBLIC;
REVOKE ALL ON DATABASE "investor" FROM PUBLIC;