package com.invest.alot.investor.test;

import static io.restassured.config.JsonConfig.jsonConfig;
import static io.restassured.config.RestAssuredConfig.newConfig;

import io.restassured.RestAssured;
import io.restassured.path.json.config.JsonPathConfig;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.boot.test.web.server.LocalServerPort;

@SuppressWarnings("java:S1075")
public class IntegrationTestBase {

  @LocalServerPort private int port;

  public String WS_URL;

  @BeforeEach
  public void setUp() {
    RestAssured.config =
        newConfig()
            .jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
    RestAssured.port = port;
    WS_URL = "ws://localhost:" + port;
  }
}
