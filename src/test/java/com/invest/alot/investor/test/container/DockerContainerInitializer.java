package com.invest.alot.investor.test.container;

import static org.springframework.boot.test.util.TestPropertyValues.of;

import com.invest.alot.investor.exception.TechnicalException;
import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Profiles;
import org.testcontainers.containers.DockerComposeContainer;
import org.testcontainers.containers.wait.strategy.Wait;
import org.testcontainers.shaded.org.apache.commons.lang3.SystemUtils;

public class DockerContainerInitializer
    implements ApplicationContextInitializer<ConfigurableApplicationContext> {

  private static DockerComposeContainer compose;
  private static Integer dbPort;

  @Override
  @SuppressWarnings("java:S2696")
  public void initialize(ConfigurableApplicationContext applicationContext) {

    if (compose == null
        && !applicationContext.getEnvironment().acceptsProfiles(Profiles.of("ci"))) {
      dbPort = getOpenPort();
      compose =
          new DockerComposeContainer<>(new File("./docker-compose.yml"))
              .withPull(false)
              .withLocalCompose(SystemUtils.IS_OS_WINDOWS)
              .withEnv("DB_PORT", String.valueOf(dbPort))
              .waitingFor("db", Wait.forLogMessage(".*ready for start up.*", 1));
      compose.start();
    }
    if (compose != null) {
      of("spring.datasource.url=jdbc:postgresql://localhost:" + dbPort + "/investor")
          .applyTo(applicationContext.getEnvironment());
    }
  }

  private Integer getOpenPort() {
    try (ServerSocket socket = new ServerSocket(0)) {
      return socket.getLocalPort();
    } catch (IOException e) {
      throw new TechnicalException(e);
    }
  }
}
