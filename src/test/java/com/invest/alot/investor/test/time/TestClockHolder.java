package com.invest.alot.investor.test.time;

import java.time.*;
import lombok.experimental.UtilityClass;

@UtilityClass
public class TestClockHolder {
  public final LocalDateTime now =
      LocalDateTime.ofInstant(Instant.ofEpochSecond(100), ZoneId.of("UTC"));
  public final Clock CLOCK = Clock.fixed(now.toInstant(ZoneOffset.UTC), ZoneId.of("UTC"));
  public final ZonedDateTime ZONED_DATE_TIME = ZonedDateTime.now(CLOCK);
}
