package com.invest.alot.investor.test;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import com.invest.alot.investor.test.container.DockerContainerInitializer;
import com.invest.alot.investor.test.time.ClockTestExecutionListener;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestConstructor;
import org.springframework.test.context.TestExecutionListeners;

@Target(ElementType.TYPE)
@Retention(RUNTIME)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ContextConfiguration(initializers = DockerContainerInitializer.class)
@TestExecutionListeners(
    listeners = {ClockTestExecutionListener.class},
    mergeMode = TestExecutionListeners.MergeMode.MERGE_WITH_DEFAULTS)
@Cleanup
@TestConstructor(autowireMode = TestConstructor.AutowireMode.ALL)
@Import({TestConfiguration.class})
@ActiveProfiles("test")
@EnableJpaAuditing
public @interface IntegrationTest {}
