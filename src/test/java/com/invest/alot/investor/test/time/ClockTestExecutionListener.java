package com.invest.alot.investor.test.time;

import com.invest.alot.investor.time.ClockHolder;
import java.time.Clock;
import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.springframework.test.context.TestContext;
import org.springframework.test.context.support.AbstractTestExecutionListener;

public class ClockTestExecutionListener extends AbstractTestExecutionListener
    implements BeforeAllCallback, AfterAllCallback {
  @Override
  public void afterAll(ExtensionContext context) throws Exception {
    ClockHolder.setClock(Clock.systemUTC());
  }

  @Override
  public void afterTestClass(TestContext testContext) throws Exception {
    ClockHolder.setClock(Clock.systemUTC());
  }

  @Override
  public void beforeAll(ExtensionContext context) throws Exception {
    ClockHolder.setClock(TestClockHolder.CLOCK);
  }

  @Override
  public void beforeTestClass(TestContext testContext) throws Exception {
    ClockHolder.setClock(TestClockHolder.CLOCK);
  }
}
