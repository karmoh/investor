package com.invest.alot.investor.test;

import com.invest.alot.investor.test.time.TestClockHolder;
import java.time.Clock;
import java.util.Optional;
import java.util.TimeZone;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.data.auditing.DateTimeProvider;

@org.springframework.boot.test.context.TestConfiguration
public class TestConfiguration {

  @Bean
  public Clock clock() {
    return TestClockHolder.CLOCK;
  }

  @Bean
  public TimeZone timeZone() {
    TimeZone defaultTimeZone = TimeZone.getTimeZone("UTC");
    TimeZone.setDefault(defaultTimeZone);
    return defaultTimeZone;
  }

  @Bean
  @Primary
  public DateTimeProvider dateTimeProvider() {
    return () -> Optional.of(TestClockHolder.ZONED_DATE_TIME);
  }
}
