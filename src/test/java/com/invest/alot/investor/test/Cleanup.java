package com.invest.alot.investor.test;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import org.springframework.test.context.jdbc.Sql;

@Sql("/data/cleanup.sql")
@Retention(RetentionPolicy.RUNTIME)
public @interface Cleanup {}
