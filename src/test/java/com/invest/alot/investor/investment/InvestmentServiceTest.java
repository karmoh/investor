package com.invest.alot.investor.investment;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.invest.alot.investor.exception.ClientException;
import com.invest.alot.investor.exception.ErrorCode;
import com.invest.alot.investor.loan.Loan;
import com.invest.alot.investor.loan.LoanService;
import com.invest.alot.investor.loan.LoanState;
import com.invest.alot.investor.socket.MessagePublisher;
import com.invest.alot.investor.test.UnitTest;
import com.invest.alot.investor.test.time.TestClockHolder;
import com.invest.alot.investor.user.User;
import com.invest.alot.investor.user.UserService;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.transaction.support.TransactionSynchronizationManager;

@UnitTest
class InvestmentServiceTest {
  private InvestmentService investmentService;

  @Mock private InvestmentRepository investmentRepository;
  @Mock private UserService userService;
  @Mock private LoanService loanService;
  @Mock private MessagePublisher messagePublisher;

  @BeforeEach
  void before() {
    TransactionSynchronizationManager.initSynchronization();
    investmentService =
        new InvestmentService(investmentRepository, userService, loanService, messagePublisher);
  }

  @AfterEach
  void after() {
    TransactionSynchronizationManager.clear();
  }

  @Test
  void investInLoan() {
    var loanId = UUID.randomUUID();
    var userId = UUID.randomUUID();
    var loan =
        Loan.builder()
            .id(loanId)
            .targetAmount(BigDecimal.valueOf(9L))
            .states(
                new ArrayList<>(
                    List.of(
                        LoanState.builder()
                            .state(LoanState.State.OPEN)
                            .createdTime(TestClockHolder.now.minusHours(1))
                            .build())))
            .build();
    var user = User.builder().id(userId).build();
    when(loanService.getLoanById(loanId)).thenReturn(loan);
    when(userService.getUserById(userId)).thenReturn(user);
    when(investmentRepository.getInvestmentSumByLoanId(loanId)).thenReturn(BigDecimal.TEN);
    when(investmentRepository.save(any())).thenAnswer(invocation -> invocation.getArgument(0));
    var investment =
        investmentService.investInLoan(
            InvestmentRequest.builder()
                .loanId(loanId)
                .userId(userId)
                .amount(BigDecimal.TEN)
                .build());
    assertThat(investment.getLoan().getId()).isEqualTo(loanId);
    assertThat(investment.getUser().getId()).isEqualTo(userId);
    assertThat(loan.getStates().size()).isEqualTo(2L);
  }

  @Test
  void investInLoanLoanClosed() {
    var loanId = UUID.randomUUID();
    var userId = UUID.randomUUID();
    var loan =
        Loan.builder()
            .id(loanId)
            .targetAmount(BigDecimal.valueOf(9L))
            .states(
                new ArrayList<>(
                    List.of(
                        LoanState.builder()
                            .state(LoanState.State.CLOSED)
                            .createdTime(TestClockHolder.now.minusHours(1))
                            .build())))
            .build();

    when(loanService.getLoanById(loanId)).thenReturn(loan);
    var exception =
        Assertions.assertThrows(
            ClientException.class,
            () ->
                investmentService.investInLoan(
                    InvestmentRequest.builder()
                        .loanId(loanId)
                        .userId(userId)
                        .amount(BigDecimal.TEN)
                        .build()));
    assertThat(exception.getErrorCode()).isEqualTo(ErrorCode.LOAN_CLOSED);
  }
}
