package com.invest.alot.investor.investment;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.*;

import com.invest.alot.investor.loan.LoanRequest;
import com.invest.alot.investor.loan.LoanState;
import com.invest.alot.investor.test.IntegrationTest;
import com.invest.alot.investor.test.IntegrationTestBase;
import com.invest.alot.investor.user.UserRequest;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.*;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;

@Slf4j
@IntegrationTest
@RequiredArgsConstructor
class InvestorIntegrationTest extends IntegrationTestBase {
  private static final String LOANS_PATH = "/loans/v1";
  private static final String USERS_PATH = "/users/v1";
  private static final String INVESTMENTS_PATH = "/investments/v1";

  @Test
  @SneakyThrows
  public void testInvestmentFlow() {

    var stompClient = new WebSocketStompClient(new StandardWebSocketClient());
    stompClient.setMessageConverter(new MappingJackson2MessageConverter());

    var receivedMessageFuture = new CompletableFuture<BigDecimal>();

    var session =
        stompClient
            .connectAsync(WS_URL + "/ws", new StompSessionHandlerAdapter() {})
            .get(1, TimeUnit.SECONDS);

    var loanName = UUID.randomUUID().toString();
    var username = UUID.randomUUID().toString();

    RestAssured.with()
        .contentType(ContentType.JSON)
        .body(UserRequest.builder().build())
        .post(USERS_PATH)
        .then()
        .log()
        .all()
        .statusCode(400)
        .body("errorCode", equalTo("invalid_params"));

    var userId =
        RestAssured.with()
            .contentType(ContentType.JSON)
            .body(UserRequest.builder().username(username).build())
            .post(USERS_PATH)
            .then()
            .log()
            .all()
            .statusCode(201)
            .body("id", notNullValue())
            .body("username", equalTo(username))
            .extract()
            .path("id")
            .toString();

    RestAssured.with()
        .contentType(ContentType.JSON)
        .body(
            LoanRequest.builder()
                .name(loanName)
                .interestRate(BigDecimal.valueOf(2.5))
                .periodInMonths(0)
                .targetAmount(BigDecimal.valueOf(20))
                .build())
        .post(LOANS_PATH)
        .then()
        .log()
        .all()
        .statusCode(400)
        .body("errorCode", equalTo("invalid_params"));

    var loanId =
        RestAssured.with()
            .contentType(ContentType.JSON)
            .body(
                LoanRequest.builder()
                    .name(loanName)
                    .interestRate(BigDecimal.valueOf(2.5))
                    .periodInMonths(12)
                    .targetAmount(BigDecimal.valueOf(20))
                    .build())
            .post(LOANS_PATH)
            .then()
            .log()
            .all()
            .statusCode(201)
            .body("id", notNullValue())
            .extract()
            .path("id")
            .toString();

    session.subscribe(
        "/topic/investedAmount/" + loanId,
        new StompFrameHandler() {
          @Override
          public @NotNull Type getPayloadType(@NotNull StompHeaders headers) {
            return BigDecimal.class;
          }

          @Override
          public void handleFrame(@NotNull StompHeaders headers, Object payload) {
            log.info("Received frame {}", payload);
            receivedMessageFuture.complete((BigDecimal) payload);
          }
        });

    var investmentId =
        RestAssured.with()
            .contentType(ContentType.JSON)
            .body(
                InvestmentRequest.builder()
                    .userId(UUID.fromString(userId))
                    .loanId(UUID.fromString(loanId))
                    .amount(BigDecimal.TEN)
                    .build())
            .post(INVESTMENTS_PATH)
            .then()
            .log()
            .all()
            .statusCode(201)
            .body("id", notNullValue())
            .extract()
            .path("id")
            .toString();

    var receivedMessage = receivedMessageFuture.get(10, TimeUnit.SECONDS);
    assertThat(receivedMessage).isEqualTo(BigDecimal.TEN);

    RestAssured.with()
        .contentType(ContentType.JSON)
        .get(LOANS_PATH)
        .then()
        .log()
        .all()
        .statusCode(200)
        .body("[0].id", equalTo(loanId))
        .body("[0].name", equalTo(loanName))
        .body("[0].state", equalTo(LoanState.State.OPEN.name()))
        .body("[0].interestRate", equalTo(BigDecimal.valueOf(2.5)))
        .body("[0].periodInMonths", equalTo(12))
        .body("[0].targetAmount", equalTo(20))
        .body("[0].investedAmount", equalTo(10))
        .body("[0].createdTime", equalTo("1970-01-01T00:01:40"));

    RestAssured.with()
        .contentType(ContentType.JSON)
        .get(LOANS_PATH + "/" + loanId)
        .then()
        .log()
        .all()
        .statusCode(200)
        .body("id", equalTo(loanId))
        .body("name", equalTo(loanName))
        .body("state", equalTo(LoanState.State.OPEN.name()))
        .body("interestRate", equalTo(BigDecimal.valueOf(2.5)))
        .body("periodInMonths", equalTo(12))
        .body("targetAmount", equalTo(20))
        .body("investedAmount", equalTo(10))
        .body("createdTime", equalTo("1970-01-01T00:01:40"))
        .body("updatedTime", equalTo("1970-01-01T00:01:40"))
        .body("investments[0].id", equalTo(investmentId))
        .body("investments[0].userId", equalTo(userId))
        .body("investments[0].username", equalTo(username))
        .body("investments[0].amount", equalTo(10))
        .body("investments[0].createdTime", equalTo("1970-01-01T00:01:40"));

    RestAssured.with()
        .contentType(ContentType.JSON)
        .get(USERS_PATH + "/" + userId)
        .then()
        .log()
        .all()
        .statusCode(200)
        .body("id", equalTo(userId))
        .body("username", equalTo(username))
        .body("loans[0].id", equalTo(loanId))
        .body("loans[0].name", equalTo(loanName))
        .body("loans[0].interestRate", equalTo(BigDecimal.valueOf(2.5)))
        .body("loans[0].periodInMonths", equalTo(12))
        .body("loans[0].targetAmount", equalTo(20))
        .body("loans[0].totalInvestedAmount", equalTo(10))
        .body("loans[0].userInvestedAmount", equalTo(10))
        .body("loans[0].userInvestedCount", equalTo(1))
        .body("loans[0].state", equalTo(LoanState.State.OPEN.name()));
  }
}
